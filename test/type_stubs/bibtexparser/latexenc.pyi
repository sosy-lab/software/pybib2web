# This file is part of pybib2html, a translator of BibTeX to HTML.
# https://gitlab.com/sosy-lab/software/pybib2html
#
# SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from _typeshed import Incomplete

__all__ = [
    "string_to_latex",
    "latex_to_unicode",
    "protect_uppercase",
    "unicode_to_latex",
    "unicode_to_crappy_latex1",
    "unicode_to_crappy_latex2",
]

def string_to_latex(string): ...
def latex_to_unicode(string): ...
def protect_uppercase(string): ...
def _replace_all_latex(string): ...

unicode_to_latex: Incomplete
unicode_to_crappy_latex1: Incomplete
unicode_to_crappy_latex2: Incomplete
