<!--
This file is part of pybib2html, a translator of BibTeX to HTML.
https://gitlab.com/sosy-lab/software/pybib2html

SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Development

## Distribution

To distribute a new version of pybib2web to [PyPi][PyPi],
follow the below steps (taken and adjusted from [the BenchExec repository](https://github.com/sosy-lab/benchexec/blob/8392b0b00a80b1ba49eb02fef63b3385a9301988/doc/DEVELOPMENT.md#releasing-a-new-version)):

0. Make sure that CHANGELOG.md is up-to-date.

1. Update version number in field `__version__` of `pybib2web/version.py`,
   e.g., from `1.1-dev` to `1.1`, and update the version information in `CHANGELOG.md`.
   Update the example in `doc/example`. See the CI check how to do this.

2. Create a Git tag with `git tag -s <VERSION>`. For example, `git tag -s 1.1`.

3. Push commits and tag to GitLab:

        git push --tags

   The GitLab CI will publish the new version to PyPi when the CI succeeds.

4. On GitLab, create a release from the tag with a description of the changes
   (from `CHANGELOG.md`), and upload all files from `dist/` for the release.

5. Update version number in field `__version__` of `pybib2web/version.py`,
   e.g., from `1.1` to `1.2-dev` and commit.


[PyPi]: https://pypi.org/
