# This file is part of pybib2html, a translator of BibTeX to HTML.
# https://gitlab.com/sosy-lab/software/pybib2html
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from . import bibdatabase as bibdatabase, bparser as bparser, bwriter as bwriter
from typing import Any
from io import FileIO

def loads(bibtex_str: str, parser: bparser.BibTexParser | None = ...): ...
def load(bibtex_file: FileIO, parser: bparser.BibTexParser | None = ...): ...
def dumps(bib_database: bibdatabase.BibDatabase, writer: FileIO | None = ...): ...
def dump(
    bib_database: bibdatabase.BibDatabase, bibtex_file, writer: FileIO | None = ...
) -> None: ...
